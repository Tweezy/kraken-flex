import logging
import os
from requests import get, post, HTTPError
from functools import wraps


dir_path = os.path.dirname(os.path.realpath(__file__))
with open(f'{dir_path}/api-key.txt') as key:
    headers = {
        'Content-Type': 'application/json',
        'x-api-key': key.read()
    }


def error_handler(func):
    ''' Error handler decorator to catch for HTTP and general exceptions. '''
    @wraps(func)
    def handler(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except HTTPError as httpError:
            logging.error(f'A HTTP error has occured: {httpError}')
            raise httpError
        except Exception as ex:
            logging.error(f'An error has occured: {ex}')
            raise ex
    return handler


@error_handler
def get_all_outages(url) -> dict:
    ''' Get to receive all outages '''
    site_url = f'{url}/outages'
    logging.info(f'Getting all outages: {site_url}')
    outages = get(site_url, headers=headers)
    outages.raise_for_status()
    return outages.json()


@error_handler
def get_site_info(url, site) -> dict:
    ''' Get request to receive site information for a specific site '''
    site_url = f'{url}/site-info/{site}'
    logging.info(f'Getting site info: {site_url}')
    outages = get(site_url, headers=headers)
    outages.raise_for_status()
    return outages.json()


@error_handler
def post_outage_information(url, site, data):
    ''' Post outages information to the site-outages for a specified site '''
    site_url = f'{url}/site-outages/{site}'
    logging.info(f'Posting site information to: {site_url}')
    request = post(site_url, json=data, headers=headers)
    request.raise_for_status()
