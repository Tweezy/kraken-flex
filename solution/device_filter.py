import logging
from datetime import datetime
from dateutil import parser, tz


def filter_devices(outages: list, site_info: dict):
    '''For every device, return all valid outages'''
    filtered_outages = []
    for device in site_info['devices']:
        valid_outages = get_all_valid_outages(device, outages)
        [filtered_outages.append(outage) for outage in valid_outages]
    return filtered_outages


def get_all_valid_outages(device: dict, outages: list):
    '''Returns a list of valid outages with device name'''
    return [
        dict(**outage, name=device['name'])
        for outage in outages
        if is_valid_outage(device['id'], outage)
    ]


def is_valid_outage(device_id: str, outage: dict):
    '''
    A valid device is one which has an id in the site_info list
    and has a begin date greater then 2022-01-01T00:00:00.000Z
    '''
    if not device_id == outage['id']:
        logging.debug('Skipping. Device ID and Outage ID do not match')
        return False

    if parser.parse(outage['begin']) < datetime(2022, 1, 1, tzinfo=tz.tzutc()):
        logging.debug('Skipping. Devices last outage not within date range.')
        return False

    return True
