import logging
import os
from yaml import load, FullLoader


class SwaggerParser:
    '''
    Opens up the api.yaml file so we can parse important information from it.

    This infromation is then used throughout the solution; e.g. the server url
    '''
    server_specification: dict = None

    def __init__(self):
        logging.info('Parsing api.yaml file')
        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(f'{dir_path}/api.yaml') as api_yaml:
            self.server_specification = load(api_yaml, Loader=FullLoader)

    def get_server_url(self) -> str:
        logging.info('Getting server url')
        return self.server_specification['servers'][0]['url']
