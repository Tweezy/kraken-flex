import logging
from kraken_requests import (
    get_all_outages, get_site_info, post_outage_information
)
from swagger_parser import SwaggerParser
from device_filter import filter_devices


logging.basicConfig(
    format='%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=logging.INFO
)


if __name__ == "__main__":
    logging.info('Entry point')
    url = SwaggerParser().get_server_url()

    site_key = 'norwich-pear-tree'
    outages = get_all_outages(url)
    site_info = get_site_info(url, site_key)

    logging.info('Filtering all devices')
    filtered_devices = filter_devices(outages, site_info)

    logging.info('Posting outage information')
    post_outage_information(url, site_key, filtered_devices)

    logging.info('Outage information sent to KrakenFlex server')
