from device_filter import (
    filter_devices, get_all_valid_outages, is_valid_outage
)


def test_returns_list_of_filtered_devices(mocker):
    # Given
    outages = [
        {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "begin": "2022-01-26T17:09:31.036Z",
            "end": "2022-04-29T00:37:42.253Z"
        },
        {
            "id": "04ccad00-eb8d-4045-8994-b569cb4b64c1",
            "begin": "2022-05-23T12:21:27.377Z",
            "end": "2022-11-13T02:16:38.905Z"
        }
    ]
    site_info = {
        "id": "kingfisher",
        "name": "KingFisher",
        "devices": [
            {
                "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
                "name": "Battery 1"
            },
            {
                "id": "086b0d53-b311-4441-aaf3-935646f03d4d",
                "name": "Battery 2"
            }
        ]
    }
    # When
    actual = filter_devices(outages, site_info)

    # Then
    expected = [
        {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "begin": "2022-01-26T17:09:31.036Z",
            "end": "2022-04-29T00:37:42.253Z",
            "name": "Battery 1"
        }
    ]
    assert actual == expected


def test_get_all_valid_outages_appends_name_to_valid_outages(mocker):
    # Given
    mocker.patch('device_filter.is_valid_outage', return_value=True)
    device = {
        'id': 'id',
        'name': 'Battery 1'
    }
    outages = [
        {
            'id': 'some-id',
        },
        {
            'id': 'some-id',
        },
    ]

    # When
    actual = get_all_valid_outages(device, outages)
    expected = [
        {
            'id': 'some-id',
            'name': 'Battery 1'
        },
        {
            'id': 'some-id',
            'name': 'Battery 1'
        }
    ]

    # Then
    assert actual == expected


def test_get_all_valid_outages_returns_empty_array(mocker):
    # Given
    mocker.patch('device_filter.is_valid_outage', return_value=False)
    device = {
        'id': 'id',
        'name': 'Battery 1'
    }
    outages = [
        {
            'id': 'some-id',
        },
        {
            'id': 'some-id',
        },
    ]

    # When
    actual = get_all_valid_outages(device, outages)
    expected = []

    # Then
    assert actual == expected


def test_is_valid_outage_returns_false_for_mismatched_ids():
    # Given
    device_id = '1'
    outage = {
        'id': 2
    }

    # When
    actual = is_valid_outage(device_id, outage)

    # Then
    assert actual is False


def test_is_valid_outage_returns_false_for_incorrect_date_range():
    # Given
    device_id = '1'
    outage = {
        'id': '1',
        'begin': '2021-01-01T00:00:00.000Z'
    }

    # When
    actual = is_valid_outage(device_id, outage)

    # Then
    assert actual is False


def test_is_valid_outage_returns_true_for_valid_outage():
    # Given
    device_id = '1'
    outage = {
        'id': '1',
        'begin': '2022-01-01T00:00:00.000Z'
    }

    # When
    actual = is_valid_outage(device_id, outage)

    # Then
    assert actual is True
