from swagger_parser import SwaggerParser


def api_yaml_config():
    return {
        'servers': [
            {
                'url': 'api.example.com'
            }
        ]
    }


def test_get_api_url(mocker):
    # Given
    parser = SwaggerParser()
    mocker.patch.object(parser, 'server_specification', api_yaml_config())

    # When
    actual = parser.get_server_url()

    # Then
    assert actual == 'api.example.com'
