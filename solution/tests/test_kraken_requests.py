import pytest
from requests.exceptions import HTTPError
from kraken_requests import (
    error_handler, get_all_outages, get_site_info, post_outage_information
)


def mocked_bad_response(mocker, status, error):
    mocked_resp = mocker.Mock()
    exception = HTTPError(
        error,
        response={
            'status_code': status,
            'reason': error
        }
    )
    mocked_resp.ok = False
    mocked_resp.status_code = status
    mocked_resp.reason = error
    mocked_resp.raise_for_status.side_effect = exception
    return mocked_resp


def mocked_good_response(mocker, json):
    mocked_resp = mocker.Mock()
    mocked_resp.ok = True
    mocked_resp.status_code = 200
    mocked_resp.json.return_value = json
    return mocked_resp


@pytest.mark.parametrize('status_code, error', [
    (400, 'Malformed request'),
    (403, 'Permission issue'),
    (404, 'Does not exist'),
    (429, 'API limit'),
    (500, 'Server error')
])
def tests_error_handler_catches_http_error(mocker, status_code, error):
    # Given
    log_mocker = mocker.patch('kraken_requests.logging')

    @error_handler
    def test_function(url):
        raise HTTPError(
            error,
            response={
                'status_code': status_code,
                'reason': error
            }
        )

    # When
    with pytest.raises(HTTPError):
        test_function('something.api')

    # Then
    log_mocker.error.assert_called_once_with(
        f'A HTTP error has occured: {error}'
    )


def test_error_handler_catches_generic_exception(mocker):
    # Given
    log_mocker = mocker.patch('kraken_requests.logging')

    @error_handler
    def test_function(url):
        raise Exception

    # When
    with pytest.raises(Exception):
        test_function('something.api')

    # Then
    log_mocker.error.assert_called_once_with(
        'An error has occured: '
    )


def test_get_all_outages_returns_outages(mocker):
    # Given
    outages = [{
        'id': '002b28fc-283c-47ec-9af2-ea287336dc1b',
        'begin': '2021-07-26T17:09:31.036Z',
        'end': '2021-08-29T00:37:42.253Z'
    }]
    mocker.patch('kraken_requests.headers', 'api-key')
    server_response = mocked_good_response(mocker, outages)
    get_request = mocker.patch(
        'kraken_requests.get', return_value=server_response)

    # When
    actual = get_all_outages('something.api')

    # Then
    get_request.assert_called_once_with(
        'something.api/outages',
        headers='api-key'
    )
    assert actual == outages


def test_get_site_info_returns_correctly(mocker):
    # Given
    mocker.patch('kraken_requests.headers', 'api-key')
    site_info = {
        'id': 'kingfisher',
        'name': 'KingFisher',
        'devices': [
            {
                'id': '002b28fc-283c-47ec-9af2-ea287336dc1b',
                'name': 'Battery 1'
            }
        ]
    }
    server_response = mocked_good_response(mocker, site_info)
    get_request = mocker.patch(
        'kraken_requests.get', return_value=server_response)

    # When
    actual = get_site_info('something.api', 'kingfisher')

    # Then
    get_request.assert_called_once_with(
        'something.api/site-info/kingfisher',
        headers='api-key'
    )
    assert actual == site_info


def test_post_site_outages_calls_correctly(mocker):
    # Given
    mocker.patch('kraken_requests.headers', 'api-key')
    payload = [
        {
            'id': '002b28fc-283c-47ec-9af2-ea287336dc1b',
            'name': 'Battery 1',
            'begin': '2022-12-04T09:59:33.628Z',
            'end': '2022-12-12T22:35:13.815Z'
        },
        {
            'id': '086b0d53-b311-4441-aaf3-935646f03d4d',
            'name': 'Battery 2',
            'begin': '2022-07-12T16:31:47.254Z',
            'end': '2022-10-13T04:05:10.044Z'
        }
    ]
    server_response = mocked_good_response(mocker, {})
    post_mock = mocker.patch(
        'kraken_requests.post', return_value=server_response
    )

    # When
    post_outage_information('something.api', 'kingfisher', payload)

    # Then
    post_mock.assert_called_once_with(
        'something.api/site-outages/kingfisher',
        headers='api-key',
        json=payload
    )
