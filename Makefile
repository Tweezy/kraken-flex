pipenv:
	pipenv install --dev

lint:
	pipenv run flake8

test:
	pipenv run pytest

run: pipenv
	pipenv run python solution/main.py

run-solution: pipenv lint test run