# KrakenFlex Technical test
This solution was created using TDD, where my first objective was to correctly get pytest and pytest-mock configurd. Once done, for every python file I would then create my test file and markup my potential functionality with my test functions, where I would then directly use my tests to implement and debug my functionality.

## Installation guide
 - This was programming in WSL2: Ubuntu-18.04. I do not think it will work on a Windows device. Unix should be fine.
 - Before we can setup our environment, you need to have python installed, any version >= 3.8
 - Once python is installed, run the following pip install command to install pipenv; `pip install pipenv`. I decided on pipenv as I find it easier to manage dependencies, bundling etc. It's also easier to define makefile commands to run linting, unit tests, python files too.
- Now that we have the local requirements met, we can run the solution on two ways;
    - linting + unit tests + solution; `make run-solution`
    - Just the solution `make run`

